﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DragModule : MonoBehaviour
{
    Vector3 startPos;
    Vector3 endPos;
    Vector3? dir;

    public Action<Vector2> OnDrag;

    void Update()
    {
        if ( dir.HasValue )
            dir = null;

        if ( Input.GetMouseButtonDown( 0 ) )
        {
            startPos = Camera.main.ScreenToViewportPoint( Input.mousePosition );
        }
        else if ( Input.GetMouseButtonUp( 0 ) )
        {
            endPos = Camera.main.ScreenToViewportPoint( Input.mousePosition );
            dir = ConvertToAtomicDir( endPos - startPos);

            if (OnDrag != null)
                OnDrag(dir.Value);
        }
    }

    Vector2 ConvertToAtomicDir(Vector2 viewportDelta)
    {
        float threshold = 0.05f;

        if (viewportDelta.magnitude > threshold )
        {
            if ( Mathf.Abs(viewportDelta.x) > Mathf.Abs( viewportDelta.y) )
            {
                return Vector2.right * Mathf.Sign( viewportDelta.x );
            }
            else
            {
                return Vector2.up * Mathf.Sign( viewportDelta.y );
            } 
        }
        else
        {
            return Vector2.zero;
        }
    }

    public bool HasDraggedInDirection(Vector2 direction)
    {
        return (dir.HasValue ? dir == direction : false);
    }
}
