﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using LiteNetLib;
using LiteNetLib.Utils;

[Serializable]
public class State
{
    public long Frame;

    public PlayerState Player1;
    public PlayerState Player2;
}

[Serializable]
public class PlayerState
{
    public float Pos;
    public int MoveID;
    public int MoveFrame;
}
