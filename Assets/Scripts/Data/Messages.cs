﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiteNetLib.Utils;
using System;

public class InputState
{
    public bool IsPlayer1 { get; set; }
    public long Frame { get; set; }
    public int MoveID { get; set; }
}

public class StartMessage
{
    public long TimeOfStart { get; set; }
}

public class SyncMessage
{
    public long RemoteFrame { get; set; }
    public long RollbackValue { get; set; }
}