﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Client : IInitializable, ITickable, IFixedTickable
{
    [Inject]
    bool _isPlayer1;

    [Inject(Id = "1")]
    StateRenderer _player1Renderer;

    [Inject(Id = "2")]
    StateRenderer _player2Renderer;

    [Inject]
    InputMgr _input;

    [Inject]
    SimulationMgr _simulation;

    [Inject]
    Text _frameCount;

    public bool Enabled = false;
    public Action<InputState> OnInput;
    public Action<SyncMessage> OnSyncMsg;

    SyncMessage _syncMsgForThisFrame;
    InputState _enemyInputReceivedThisFrame;
    int _lastMoveID;

    public void Initialize()
    {
        Render();
    }

    public void Tick()
    {
        if (!Enabled)
            return;

        int moveID = _input.GetInputForFrame(_isPlayer1);

        if (moveID != 0)
            _lastMoveID = moveID;
    }

    public void FixedTick()
    {
        if (!Enabled)
            return;
        
        State state = _simulation.GetState();
        
        // rewind and replay if needed

        if (_enemyInputReceivedThisFrame != null)
        {
            _simulation.RewindAndReplay(_enemyInputReceivedThisFrame);
        }

        _enemyInputReceivedThisFrame = null;

        // fire animation input

        if (_lastMoveID != 0)
        {
            myInputCount++;
            InputState input = new InputState { MoveID = _lastMoveID, Frame = state.Frame + 1, IsPlayer1 = _isPlayer1 };
            _simulation.BufferInput(input);

            if (OnInput != null)
                OnInput(input);

            _lastMoveID = 0;
        }
        
        // exec frame

        if (_syncMsgForThisFrame != null && ShouldWait())
        {
            Debug.Log("waiting one frame");
        }
        else
        {
            _simulation.ApplyFrame();
        }

        _syncMsgForThisFrame = null;
        
        // render

        Render();

        // send sync msg

        if (state.Frame % 3 == 0)
        {
            if (OnSyncMsg != null)
                OnSyncMsg(new SyncMessage { RemoteFrame = state.Frame, RollbackValue = rollbackValue });
        }
    }


    public void HandleSyncMsg(SyncMessage syncMsg)
    {
        _syncMsgForThisFrame = syncMsg;
        rollbackValue = _simulation.GetState().Frame - _syncMsgForThisFrame.RemoteFrame;
        remoteRollbackValue = _syncMsgForThisFrame.RollbackValue;
    }

    long myInputCount = 0;
    long enemyInputCount = 0;

    public void HandleEnemyInput(InputState incomingInput)
    {
        _enemyInputReceivedThisFrame = incomingInput;
        enemyInputCount++;
    }

    public void Render()
    {
        State state = _simulation.GetState();
        //_frameCount.text = JsonConvert.SerializeObject(state, Formatting.Indented);
        _frameCount.text = _isPlayer1 + " " + state.Frame;
        _player1Renderer.Render(state.Player1);
        _player2Renderer.Render(state.Player2);
    }

    long rollbackValue;
    long remoteRollbackValue;

    public bool ShouldWait()
    {
        return (rollbackValue > remoteRollbackValue + 1); // ex : 1 > 1 + 1, but everytime we have 8 > 12 on both
    }

    public class Factory : Factory<bool, Client>
    {
    }
}
