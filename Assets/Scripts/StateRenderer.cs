﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateRenderer : MonoBehaviour
{
    [SerializeField]
    Transform _view;

    [SerializeField]
    AnimationRenderer _anim;
    
    public void Render(PlayerState currentState)
    {
        // position

        Vector3 pos = _view.position;
        pos.x = currentState.Pos;
        _view.position = pos;

        // animation
        
        int _animHash = _anim.Idle;

        if (currentState.MoveID == 1) _animHash = _anim.Forward;
        else if (currentState.MoveID == 2) _animHash = _anim.Back;
        else if (currentState.MoveID == 3) _animHash = _anim.High;
        else if (currentState.MoveID == 4) _animHash = _anim.Low;
        else if (currentState.MoveID == 5) _animHash = _anim.Hurt;

        _anim.Play(_animHash, currentState.MoveFrame * Time.fixedDeltaTime);
    }
}
