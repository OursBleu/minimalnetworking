﻿
using System.Collections.Generic;
using UnityEngine;

public enum Moves
{
    idle = 0,
    forward = 1,
    back = 2,
    high = 3,
    low = 4,
    hurt = 5
}

public class BakedAnimations
{
    public Dictionary<int, BakedAnimation> Animations = new Dictionary<int, BakedAnimation>();
}

public class BakedAnimation
{
    public List<BakedFrame> Frames = new List<BakedFrame>();

    public BakedFrame GetNearest(float normalizedTime)
    {
        return Frames[Mathf.RoundToInt(normalizedTime * Frames.Count)];
    }
}

public class BakedFrame
{
    public List<RectCoord> HitBoxes = new List<RectCoord>();
    public List<RectCoord> HurtBoxes = new List<RectCoord>();
}

public class RectCoord
{
    public float x;
    public float y;
    public float width;
    public float height;

    public RectCoord(float x, float y, float width, float height)
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public bool IsColliding(RectCoord rect2)
    {
        return
            IsPointInRect(x - (width * .5f), y + (width * .5f), rect2) ||
            IsPointInRect(x + (width * .5f), y + (width * .5f), rect2) ||
            IsPointInRect(x + (width * .5f), y - (width * .5f), rect2) ||
            IsPointInRect(x - (width * .5f), y - (width * .5f), rect2);
    }

    public bool IsPointInRect(float x, float y, RectCoord rect)
    {
        return
            rect.x - (rect.width * .5f) < x && x < rect.x + (rect.width * .5f) &&
            rect.y - (rect.height * .5f) < y && y < rect.y + (rect.height * .5f);
    }

    //public bool IsColliding(RectCoord rect2)
    //{
    //    return
    //        x < rect2.x + rect2.width && x + width > rect2.x &&
    //       y < rect2.y + rect2.height && height + y > rect2.y;
    //}

    public RectCoord Transform(float xPos, bool inverted)
    {
        return new RectCoord( (x * (inverted ? -1f : 1f)) + xPos,  y, width,  height);
    }
}
