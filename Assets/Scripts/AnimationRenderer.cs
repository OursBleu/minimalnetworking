﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRenderer : MonoBehaviour
{
    [SerializeField]
    Animator _animator;

    public readonly int Idle = Animator.StringToHash("idle");
    public readonly int Forward = Animator.StringToHash("forward");
    public readonly int Back = Animator.StringToHash("back");
    public readonly int High = Animator.StringToHash("high");
    public readonly int Low = Animator.StringToHash("low");
    public readonly int Hurt = Animator.StringToHash("hurt");

    public void Play(int hashName, float normalizedTime)
    {
        _animator.Play(hashName, 0, normalizedTime);
        _animator.Update(0f);
    }

    bool IsPlaying(int hash)
    {
        return _animator.GetCurrentAnimatorStateInfo(0).shortNameHash != hash;
    }
}
