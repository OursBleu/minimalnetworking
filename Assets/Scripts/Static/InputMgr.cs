﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;

public class InputMgr
{
    [Inject]
    DragModule _dragModule;

    public int GetInputForFrame (bool isPlayer1)
    {
        int moveID = 0;

        if (isPlayer1)
        {
            if (_dragModule.HasDraggedInDirection(Vector2.right)) moveID = 1;
            else if (_dragModule.HasDraggedInDirection(Vector2.left)) moveID = 2;
            else if (_dragModule.HasDraggedInDirection(Vector2.up)) moveID = 3;
            else if (_dragModule.HasDraggedInDirection(Vector2.down)) moveID = 4;
        }
        else
        {
            if (_dragModule.HasDraggedInDirection(Vector2.left)) moveID = 1;
            else if (_dragModule.HasDraggedInDirection(Vector2.right)) moveID = 2;
            else if (_dragModule.HasDraggedInDirection(Vector2.up)) moveID = 3;
            else if (_dragModule.HasDraggedInDirection(Vector2.down)) moveID = 4;
        }
        
        return moveID;
    }
}
