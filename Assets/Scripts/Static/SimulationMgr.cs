﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Zenject;

public class SimulationMgr
{
    public const float SPEED = 1f;
    public const float DASH_SPEED = 4f;
    public const float BACKDASH_SPEED = -2f;
    public const float HURT_SPEED = -4f;
    public const int ANIM_DURATION = 34;
    public const float PLAYER_WIDTH = 1f;

    [Inject]
    State _currentState;

    [Inject]
    CollisionMgr _collisions;

    public List<InputState> _bufferedInputs = new List<InputState>();
    public Dictionary<long, State> _previousStates = new Dictionary<long, State>();
    public long _lastSyncedFrame;

    public State GetState()
    {
        return _currentState;
    }

    public void ApplyFrame()
    {
        // time

        _currentState.Frame++;

        // our inputs for the frame

        foreach (var input in _bufferedInputs.FindAll(x => x.Frame == _currentState.Frame)) // filter enemy inputs ?
        {
            ApplyInput(input);
        }

        // simulation

        float distanceBetweenPlayers = Mathf.Abs(_currentState.Player1.Pos - _currentState.Player2.Pos);

        for (int playerIndex = 1; playerIndex <= 2; playerIndex++)
        {
            PlayerState playerState = playerIndex == 1 ? _currentState.Player1 : _currentState.Player2;
            PlayerState otherPlayerState = playerIndex == 1 ? _currentState.Player2 : _currentState.Player1;
            int playerDir = playerIndex == 1 ? 1 : -1;

            // speed modifiers

            float speedModifier = 0f;

            if (playerState.MoveID == 0)
                speedModifier = SPEED;
            else if (playerState.MoveID == 1)
                speedModifier = DASH_SPEED;
            else if (playerState.MoveID == 2)
                speedModifier = BACKDASH_SPEED;
            else if (playerState.MoveID == 5)
                speedModifier = HURT_SPEED;

            playerState.Pos += (speedModifier * playerDir) * Time.fixedDeltaTime;

            // clamp position

            if (playerIndex == 1)
                playerState.Pos = Mathf.Clamp(playerState.Pos, -6f, otherPlayerState.Pos - PLAYER_WIDTH * playerDir);
            else
                playerState.Pos = Mathf.Clamp(playerState.Pos, otherPlayerState.Pos - PLAYER_WIDTH * playerDir, 6f);

            // animation

            playerState.MoveFrame++;

            if (playerState.MoveFrame == ANIM_DURATION)
            {
                playerState.MoveID = 0;
                playerState.MoveFrame = 0;
            }

            // collisions

            int result = _collisions.Simulate(_currentState);
            if (result != 0)
            {
                PlayerState player = result == 1 ? _currentState.Player1 : _currentState.Player2;
                if (player.MoveID != 5)
                {
                    player.MoveID = 5;
                    player.MoveFrame = 0;
                }
            }
        }

        _previousStates[_currentState.Frame] = _currentState.Copy();
    }

    void ApplyInput(InputState input)
    {
        PlayerState playerState = input.IsPlayer1 ? _currentState.Player1 : _currentState.Player2;

        if (playerState.MoveID != 0)
            return;

        // animation

        playerState.MoveID = input.MoveID;
        playerState.MoveFrame = 0;
    }

    public void BufferInput(InputState input)
    {
        _bufferedInputs.Add(input);
    }

    public void RewindAndReplay(InputState incomingInput)
    {
        long localFrame = _currentState.Frame;
        _lastSyncedFrame = incomingInput.Frame;
        Debug.LogFormat("local {0} - remote {1}", _currentState.Frame, incomingInput.Frame);

        // buffer enemy input

        BufferInput(incomingInput);

        // rewind state if input is coming from the past

        if (localFrame >= incomingInput.Frame)
        {
            for (long i = incomingInput.Frame; i <= localFrame; i++)
            {
                _previousStates.Remove(i);
            }

            _currentState = _previousStates[incomingInput.Frame - 1];

            // replay frames

            for (long i = incomingInput.Frame; i <= localFrame; i++)
            {
                // execute next frame with these inputs

                ApplyFrame();
            }
        }
    }
}
