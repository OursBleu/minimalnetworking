﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Timers;
using System.Net.Sockets;
using System.Net;
using LiteNetLib;
using UnityEngine.UI;

public class Game : IInitializable
{
    [Inject]
    GameClient.Factory _networkFactory;

    [Inject]
    Client.Factory _playerFactory;

    [Inject]
    TickableManager _tickManager;

    [Inject]
    DisposableManager _disposableManager;

    [Inject]
    DragModule _dragModule;

    [Inject(Id = "1")]
    Button _selectPlayer1;

    [Inject(Id = "2")]
    Button _selectPlayer2;

    GameClient _network;
    Client _player;

    public void Initialize()
    {
        _selectPlayer1.onClick.AddListener(() => Select(true));
        _selectPlayer2.onClick.AddListener(() => Select(false));
    }

    void Select(bool isPlayer1)
    {
        _selectPlayer1.gameObject.SetActive(false);
        _selectPlayer2.gameObject.SetActive(false);

        // creation

        _network = _networkFactory.Create(isPlayer1);
        _network.Initialize();
        _tickManager.Add(_network);
        _disposableManager.Add(_network);

        _player = _playerFactory.Create(isPlayer1);
        _player.Initialize();
        _tickManager.Add(_player);
        _tickManager.AddFixed(_player);

        // event binding

        if (isPlayer1)
        {
            StartMessage msg = new StartMessage { TimeOfStart = DateTime.UtcNow.AddSeconds(3d).Ticks };
            _network.OnConnection += () => { _network.SendMessage(msg); HandleStartMessage(msg); };
        }
        else
        {
            _network.AddMessageType<StartMessage>(HandleStartMessage);
        }

        _network.AddMessageType<InputState>(_player.HandleEnemyInput);
        _network.AddMessageType<SyncMessage>(_player.HandleSyncMsg);
        _player.OnInput += _network.SendMessage;
        _player.OnSyncMsg += _network.SendMessage;
    }

    // ---------------------------------- START SYNC --------------------------------------------
    
    void HandleStartMessage(StartMessage startMsg)
    {
        Debug.Log("STTART !");
        _player.Enabled = true;
    }

    // ---------------------------------- SYNC --------------------------------------------
    
}