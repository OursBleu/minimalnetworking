﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System;
using Newtonsoft.Json;
using System.Linq;
using System.IO;

public static class BakingMgr
{
    const string PATH = "baked_anims.json";

    [MenuItem("CONTEXT/Animator/Bake")]
    static void BakeAll(MenuCommand command)
    {
        Animator anim = command.context as Animator;

        BakedAnimations bakedAnimations = new BakedAnimations();

        Vector3 defaultPos = anim.transform.position;
        anim.transform.position = Vector3.zero;
        foreach (var state in anim.runtimeAnimatorController.animationClips)
        {
            bakedAnimations.Animations[(int)Enum.Parse(typeof(Moves), state.name)] = BakeSingle(anim.gameObject, state, Time.fixedDeltaTime);
            Debug.Log(bakedAnimations.Animations[(int)Enum.Parse(typeof(Moves), state.name)].Frames.Count);
        }
        anim.transform.position = defaultPos;

        File.WriteAllText(Application.dataPath + "/Resources/" + PATH, JsonConvert.SerializeObject(bakedAnimations, Formatting.Indented));

        Debug.Log("saved");
    }

    static BakedAnimation BakeSingle(GameObject go, AnimationClip clip, float deltaTime)
    {
        BakedAnimation backedAnim = new BakedAnimation();

        for (float time = 0f; time < clip.length; time += deltaTime)
        {
            BakedFrame frame = new BakedFrame();

            clip.SampleAnimation(go, time);

            go.transform.GetFlattenedHierarchy(t =>
            {
                BoxCollider2D collider = t.GetComponent<BoxCollider2D>();
                if (collider)
                {
                    RectCoord rect = new RectCoord(
                        t.position.x,
                        t.position.y,
                        collider.bounds.size.x,
                        collider.bounds.size.y);

                    if (collider.isTrigger)
                        frame.HitBoxes.Add(rect);
                    else
                        frame.HurtBoxes.Add(rect);
                }
            });

            backedAnim.Frames.Add(frame);
        }

        return backedAnim;
    }


    static void GetFlattenedHierarchy(this Transform t, Action<Transform> action)
    {
        Dictionary<string, RectCoord> res = new Dictionary<string, RectCoord>();
        Queue<Transform> targets = new Queue<Transform>();
        targets.Enqueue(t);

        while (targets.Count > 0)
        {
            foreach (Transform child in targets.Dequeue())
            {
                action(child);
                targets.Enqueue(child);
            }
        }
    }
}
