﻿using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using System;
using Zenject;
using UnityEngine.SceneManagement;

public class GameClient : INetEventListener, IInitializable, ITickable, IDisposable
{
    [Inject]
    bool _isPlayer1;

    public Action OnConnection;
    public float LatestLatency;

    NetManager _netClient;
    NetSerializer _serializer;

    public void Initialize()
    {
        _netClient = new NetManager(this, "sample_app");
        _netClient.Start(_isPlayer1 ? 5001 : 5002);
        _netClient.UpdateTime = 15;
        _netClient.DiscoveryEnabled = true;

        _serializer = new NetSerializer();
    }

    public void AddMessageType<TMsgType>(Action<TMsgType> messageHandler) where TMsgType : class, new()
    {
        _serializer.SubscribeReusable<TMsgType, NetPeer>((msg, peer) => messageHandler(msg));
    }

    public void SendMessage<TMsgType>(TMsgType message) where TMsgType : class, new()
    {
        NetPeer peer = _netClient.GetFirstPeer();
        peer.Send(_serializer.Serialize(message.Copy()), SendOptions.ReliableOrdered);
    }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
        _serializer.ReadAllPackets(reader, peer);
    }

    public void Tick()
    {
        _netClient.PollEvents();

        var peer = _netClient.GetFirstPeer();
        if (peer != null && peer.ConnectionState == ConnectionState.Connected)
        {

        }
        else
        {
            _netClient.SendDiscoveryRequest(new byte[] { 1 }, _isPlayer1 ? 5002 : 5001);
        }
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        if (_netClient.PeersCount > 0)
            return;

        if (messageType == UnconnectedMessageType.DiscoveryRequest)
        {
            //Debug.Log("[SERVER] Received discovery request. Send discovery response");

            _netClient.SendDiscoveryResponse(new byte[] { 1 }, remoteEndPoint);
        }
        else if (messageType == UnconnectedMessageType.DiscoveryResponse)
        {
            Debug.Log("[CLIENT] Received discovery response. Connecting to: " + remoteEndPoint);
            _netClient.Connect(remoteEndPoint);
        }
    }

    public void Dispose()
    {
        if (_netClient != null)
            _netClient.Stop();
    }

    public void OnPeerConnected(NetPeer peer)
    {
        if (OnConnection != null)
            OnConnection();

        Debug.Log("[CLIENT] We connected to " + peer.EndPoint);
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        Debug.Log("[CLIENT] We received error " + socketErrorCode);
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        LatestLatency = latency / 1000f;
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
    {
        if (_netClient != null)
            _netClient.Stop();

        Debug.Log("[CLIENT] We disconnected because " + disconnectInfo.Reason);
    }

    public class Factory : Factory<bool, GameClient>
    {
    }
}
