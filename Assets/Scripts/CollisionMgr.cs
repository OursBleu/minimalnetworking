﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class CollisionMgr : IInitializable
{
    const string PATH = "baked_anims";

    BakedAnimations bakedAnimations;

    public void Initialize()
    {
        //RectCoord hit = new RectCoord(0.202152848f, 0.0f, 0.351098061f, 0.351098061f);
        //RectCoord hurt = new RectCoord(0.5000036f, 0.0102643194f, 1.0f, 1.0f);
        //Debug.Log(hit.IsColliding(hurt));
            
        bakedAnimations = JsonConvert.DeserializeObject<BakedAnimations>(Resources.Load<TextAsset>(PATH).text);
    }

    public int Simulate(State state)
    {
        BakedFrame p1Frame = bakedAnimations.Animations[state.Player1.MoveID].Frames[state.Player1.MoveFrame];
        BakedFrame p2Frame = bakedAnimations.Animations[state.Player2.MoveID].Frames[state.Player2.MoveFrame];

        foreach (var hit in p1Frame.HitBoxes.Select(x => x.Transform(state.Player1.Pos, false)))
        {
            foreach (var hurt in p2Frame.HurtBoxes.Select(x => x.Transform(state.Player2.Pos, true)))
            {
                if (hit.IsColliding(hurt))
                {
                    return 2;
                }
            }
        }

        foreach (var hit in p2Frame.HitBoxes.Select(x => x.Transform(state.Player2.Pos, true)))
        {
            foreach (var hurt in p1Frame.HurtBoxes.Select(x => x.Transform(state.Player1.Pos, false)))
            {
                if (hit.IsColliding(hurt))
                {
                    return 1;
                }
            }
        }

        return 0;
    }
}
