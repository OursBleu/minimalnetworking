﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Server : MonoBehaviour
{
    [SerializeField]
    Client _player1;

    [SerializeField]
    Client _player2;

    [SerializeField]
    float _latency = .1f;

    [SerializeField]
    float _dropRatio = .3f;

    void Start()
    {
        _player1.OnInput += input => StartCoroutine(SendInputTo(input.Copy(), _player2));
        _player2.OnInput += input => StartCoroutine(SendInputTo(input.Copy(), _player1));
    }

    IEnumerator SendInputTo(InputState input, Client dest)
    {
        yield return new WaitForSeconds(_latency);

        dest.HandleEnemyInput(input);
    }
}
