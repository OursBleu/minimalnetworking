using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller<GameInstaller>
{
    public override void InstallBindings()
    {
        State defaultState = new State
        {
            Player1 = new PlayerState { Pos = -5 },
            Player2 = new PlayerState { Pos = 5 }
        };

        Container.Bind<State>().FromInstance(defaultState);

        Container.BindInterfacesAndSelfTo<Game>().AsSingle();
        Container.BindInterfacesAndSelfTo<CollisionMgr>().AsSingle();

        Container.Bind<SimulationMgr>().AsSingle();
        Container.Bind<InputMgr>().AsSingle();

        Container.BindFactory<bool, Client, Client.Factory>();
        Container.BindFactory<bool, GameClient, GameClient.Factory>();
    }
}